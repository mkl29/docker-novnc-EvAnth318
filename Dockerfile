FROM ubuntu:18.04
LABEL maintainer="Michael Larbi <mkl29@duke.edu>"

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /root

RUN       apt-get update   &&  apt-get dist-upgrade -y
RUN apt-get install -y --force-yes --no-install-recommends \
        python-numpy \ 
        software-properties-common \
        wget \
        supervisor \
        openssh-server \
        pwgen \
        sudo \
        iputils-ping \
        vim-tiny \
        net-tools \
        lxde \
        lxde-common \
        menu \
        openbox \
        openbox-menu \
        xterm \
        obconf \
        obmenu \
        xfce4-terminal \
        python-xdg \
        scrot \
        xvfb \
        gtk2-engines-murrine \
        ttf-ubuntu-font-family \
        firefox \
        firefox-dev \
        libwebkitgtk-3.0-0 \
        xserver-xorg-video-dummy \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*

### begin noVNC needs a better VNC server  ###
WORKDIR /opt
RUN apt update -y
RUN apt install -y \
        build-essential \
        pkg-config \
        libvncserver-dev \
        libssl-dev \
        xorg-dev \
        autoconf \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*
RUN wget https://github.com/LibVNC/x11vnc/archive/0.9.16.tar.gz
RUN tar -xvf 0.9.16.tar.gz
WORKDIR /opt/x11vnc-0.9.16
RUN autoreconf -fiv
RUN ./configure
RUN make
RUN make install
WORKDIR /
### end noVNC ###


RUN mkdir /etc/startup.aux/
RUN echo "#Dummy" > /etc/startup.aux/00.sh
RUN chmod +x /etc/startup.aux/00.sh
RUN mkdir -p /etc/supervisor/conf.d
RUN rm /etc/supervisor/supervisord.conf

# create an ubuntu user
#PASS=`pwgen -c -n -1 10`
#PASS=ubuntu
#echo "User: ubuntu Pass: $PASS"
#RUN useradd --create-home --shell /bin/bash --user-group --groups adm,sudo ubuntu

# create an ubuntu user who cannot sudo
RUN useradd --create-home --shell /bin/bash --user-group ubuntu
RUN echo "ubuntu:badpassword" | chpasswd

ADD startup.sh /
ADD cleanup-cruft.sh /
ADD supervisord.conf.xorg /etc/supervisor/supervisord.conf
EXPOSE 6080

ADD openbox-config /openbox-config
RUN cp -r /openbox-config/.config ~ubuntu/
RUN chown -R ubuntu ~ubuntu/.config ; chgrp -R ubuntu ~ubuntu/.config
RUN rm -r /openbox-config

# noVNC
RUN pwd
RUN ls -la
ADD noVNC /noVNC/
RUN ls -la /noVNC/
# make sure the noVNC self.pem cert file is only readable by root
#RUN chmod 400 /noVNC/self.pem

# store a password for the VNC service
RUN mkdir /home/root
RUN mkdir /home/root/.vnc
RUN x11vnc -storepasswd foobar /home/root/.vnc/passwd
ADD xorg.conf /etc/X11/xorg.conf


RUN apt-get update && apt-get install -yq \
 build-essential \
 cmake \
 libpng-dev \
 zlib1g-dev \
 libjpeg-dev \
 python-dev \
 imagemagick \
 python-pip \
 python-tk \
 python-numpy \
 python-scipy \
 python-matplotlib \
 git \
 python3-pip \
 python3-tk \
 python3-numpy \
 python3-scipy \
 python3-matplotlib

RUN  pip install pillow
RUN  pip3 install pillow


#Add Plink - Ubuntu 18.04
RUN apt update
RUN apt install -y plink
RUN ln -s /usr/lib/debian-med/bin/plink /usr/bin/plink 

# admixture
ADD admixture_linux-1.3.0 /admixture_linux-1.3.0
RUN cp /admixture_linux-1.3.0/admixture /usr/bin/
RUN rm -rf admixture_linux-1.3.0

#Add CRAN Repository and install R - Ubuntu 18.04
RUN apt install -y apt-transport-https software-properties-common
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
RUN add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/'
RUN apt update
RUN apt install -y r-base 

#RStudio Install
RUN apt install -y gdebi-core
RUN apt install -y libnss3
RUN wget https://download1.rstudio.org/desktop/bionic/amd64/rstudio-1.3.1093-amd64.deb
RUN apt install -y ./rstudio-1.3.1093-amd64.deb
RUN rm -f rstudio-1.3.1093-amd64.deb


# get rid of some LXDE & OpenBox cruft that doesn't work and clutters menus
RUN rm /usr/share/applications/display-im6.q16.desktop & \
    rm /usr/share/applications/display-im6.desktop & \
    rm /usr/share/applications/lxterminal.desktop & \
    rm /usr/share/applications/debian-uxterm.desktop & \
    rm /usr/share/applications/x11vnc.desktop & \
    rm /usr/share/applications/lxde-x-www-browser.desktop & \
    ln -s /usr/share/applications/firefox.desktop /usr/share/applications/lxde-x-www-browser.desktop & \
    rm /usr/share/applications/lxde-x-terminal-emulator.desktop  & \
    rm -rf /usr/share/ImageMagick-6

# add the shared data directory
RUN mkdir /data

# SLIM install
RUN apt-get update && apt-get install -yq \
      qt5-default \
	  qt5-qmake
RUN wget https://raw.githubusercontent.com/MesserLab/SLiM-Extras/master/installation/DebianUbuntuInstall.sh -O DebianUbuntuInstall.sh
RUN pwd
RUN cat ./DebianUbuntuInstall.sh
RUN /bin/bash ./DebianUbuntuInstall.sh
RUN rm ./DebianUbuntuInstall.sh

ENTRYPOINT ["/startup.sh"]
